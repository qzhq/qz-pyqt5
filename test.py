# -*-coding:  gbk -*-
"""
# File Name    : test.py
# Create Time  : 2023/11/7 0007
# Author       : QinZhou
# Email        : 1185917912@qq.com
# Described    : 
"""
# import sys
# from PyQt5.QtWidgets import QWidget, QApplication
#
# app = QApplication(sys.argv)
# widget = QWidget()
# widget.resize(640, 480)
# widget.setWindowTitle("Hello, PyQt5!")
# widget.show()
# sys.exit(app.exec())

import wmi

# m_wmi = wmi.WMI()
# cpu_info = m_wmi.Win32_Processor()
# if len(cpu_info) > 0:
#     serial_number = cpu_info[0].ProcessorId
#     print(serial_number)
#
m_wmi = wmi.WMI()
for network in m_wmi.Win32_NetworkAdapterConfiguration():
    mac_address = network.MacAddress
    if mac_address is not None:
        print(mac_address)
#
# m_wmi = wmi.WMI()
# disk_info = m_wmi.Win32_PhysicalMedia()
# if len(disk_info) > 0:
#     serial_number = disk_info[0].SerialNumber.strip()
#     print(serial_number)


m_wmi = wmi.WMI()
board_info = m_wmi.Win32_BaseBoard()
if len(board_info) > 0:
    board_id = board_info[0].SerialNumber.strip().strip('.')
    print(board_id)
